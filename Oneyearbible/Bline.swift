//
//  Sentence.swift
//  Oneyearbible
//
//  Created by Yongjung Shin on 2022/03/18.
//

import Foundation

struct Bline: Decodable {
    let idx: Int
    let cate: Int
    let book: Int
    let chapter: Int
    let paragraph: Int
    let sentence: String
    let testament: String
    let long_label: String
    let short_label: String
    
    static let example = Bline(idx: 1, cate: 1, book: 1, chapter: 1, paragraph: 1, sentence: "<천지 창조> 태초에 하나님이 천지를 창조하시니라", testament: "구", long_label: "창세기", short_label: "창")
}

