//
//  Blines.swift
//  Oneyearbible
//
//  Created by Yongjung Shin on 2022/03/18.
//

import Foundation

class Blines: ObservableObject {
    let blines: [Bline]
    
    var primary: Bline {
        blines[0]
    }
    
    init() {
        let url = Bundle.main.url(forResource: "bibleFull", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        blines = try! JSONDecoder().decode([Bline].self, from: data)
    }
}
