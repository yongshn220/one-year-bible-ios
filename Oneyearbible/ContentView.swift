//
//  ContentView.swift
//  Oneyearbible
//
//  Created by Yongjung Shin on 2022/03/16.
//

import SwiftUI

struct ContentView: View {
    @State var selection: String = "hi"
    var body: some View {
        TabView {
            TestView()
        }
        .tabViewStyle(PageTabViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
