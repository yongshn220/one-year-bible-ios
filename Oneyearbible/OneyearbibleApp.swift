//
//  OneyearbibleApp.swift
//  Oneyearbible
//
//  Created by Yongjung Shin on 2022/03/16.
//

import SwiftUI

@main
struct OneyearbibleApp: App {
    @StateObject var blines = Blines()
    
    var body: some Scene {
        WindowGroup {
            TabView {
                TestView()
                    .tabItem({
                        Image(systemName: "airplane.circle.fill")
                        Text("Read")
                    })
            }
            .environmentObject(blines)
        }
    }
}
