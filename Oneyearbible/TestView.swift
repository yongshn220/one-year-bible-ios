//
//  TestView.swift
//  Oneyearbible
//
//  Created by Yongjung Shin on 2022/03/16.
//

import SwiftUIPager
import SwiftUI

struct TestView: View {
    @EnvironmentObject var blines: Blines
    
    @StateObject var page = Page.first()
    @State var isPresented: Bool = false
    
    @State var glNum = 0
    
    var data = Array(0..<3)
    
    @State var dataList = [PieceView(txt: "a"), PieceView(txt: "b"), PieceView(txt: "c")]
    
    @State var curPageNum = 0
    @State var prevPageNum = -1
    
    @State var pageDirection = PageDirection.null
    
    enum PageDirection {
        case null
        case next
        case previous
    }
    
    var body: some View {
        NavigationView {
            GeometryReader { proxy in
                VStack {
                    Pager(page: self.page,
                          data: self.data,
                          id: \.self) {
                            self.pageViewC($0)
                    }
                    .onPageChanged(pageChangedEvent)
                    .pagingPriority(.simultaneous)
                    .loopPages()
                    .sensitivity(.high)
//                    .itemSpacing(10)
//                    .itemAspectRatio(1.3, alignment: .start)
                    .padding(10)
                    .background(Color.yellow)
                }
//                .navigationBarTitle("Infinite Pagers", displayMode: .inline)
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
    
    func pageViewC(_ page: Int) -> some View {
        dataList[page]
    }
    
    func pageChangedEvent(page: (Int)) -> Void {
        
        let pageDir = getPageDirection(page: page)

        if pageDir == PageDirection.next {
            glNum += 1
            let num1 = (page + 1) % 3
            print("next : \(num1)")
            dataList[num1] = PieceView(txt: "\(glNum + 1)")
        }
        
        else if pageDir == PageDirection.previous {
            glNum -= 1
            var num2 = -1
            if page == 0 {
                num2 = 2
            }
            else {
                num2 = page - 1
            }
            dataList[num2] = PieceView(txt: "\(glNum - 1)")
        }
        print("glNum: \(glNum)")
    }
    
    func getPageDirection(page: (Int)) -> PageDirection {
        prevPageNum = curPageNum
        curPageNum = page
        
        if prevPageNum == 0 && curPageNum == 1 ||
           prevPageNum == 1 && curPageNum == 2 ||
           prevPageNum == 2 && curPageNum == 0
        {
            print("next")
            return PageDirection.next
        }

        else if prevPageNum == 0 && curPageNum == 2 ||
                prevPageNum == 1 && curPageNum == 0 ||
                prevPageNum == 2 && curPageNum == 1
        {
            print("prev")
            return PageDirection.previous
        }
        else {
            return PageDirection.null
        }
    }
}


struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        TestView()
    }
}
